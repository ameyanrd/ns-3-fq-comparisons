# ns-3-fq-comparisons

## Steps to use the script:
1. Clone the ns-3-dev repository from [GitLab Repo](https://gitlab.com/nsnam/ns-3-dev).
2. Please follow the ns-3 manual or tutorial on how to build the source.
3. Copy the `fq-comparison.cc` file into the `scratch/` folder.
4. To run the file with default argument, please use:
```bash
./waf --run scratch/fq-comparison
```

## Topology:
```mermaid
flowchart LR
     Ping-Server--->n2
     n0-server--->n2
     n2--Bottleneck-->n3
     n3--->n4-client
     n3--->Ping-Client
```

## Parameters:
1. **bottleneckQueueType:** The Queue type used ("fqCodel" / "fqPie" / "fqCobalt") [fqPie].
2. **bottleneckDelay:** The delay on the bottleneck link ("Xms", where is X is the delay in milliseconds) [20ms].
3. **linkDelay:** The delay on the edge links ("Xms", where X is the delay in milliseconds) [10ms].
4. **bottleneckQueueType:** The TCP type used ("cubic" / "bic" / "dctcp" / "reno") [cubic].
5. **bottleneckRate:** Data rate of bottleneck link ("XMbps", where X is the data rate in Mbps) [10Mbps].
6. **linkrate:** Data rate of edge link  ("XMbps", where X is the bandwidth in Mbps) [100Mbps].
7. **tcpStreams:** The number of TCP-flows between the n0-server and n4-client ("X", where X is the number of flows) [1].
8. **useEcn:** Boolean indicating whether ECN should be enabled or disabled ("true" / "false") [true].
9. **useBql:** Boolean indicating whether BQL should be enabled or disabled ("true" / "false") [false].
10. **stopTime:** The duration of the simulation ("Xs", where X is the time in seconds) [70s].
11. **enablePcap:** To enable Pcap ("true" / "false") [false].

## Example to generate dat file:

```bash
./waf --run "scratch/fq-comparison.cc --linkDelay=5ms --bottleneckDelay=30ms --bottleneckQueueType=fqCodel --n0TcpType=cubic --bottleneckRate=80Mbps --linkRate=800Mbps --stopTime=200s --useEcn=true --bql=false --tcpStreams=1 --enablePcap=false"
```
The above bash command will generate all relavant dat files and place them inside a folder named **"fqCodel_1_80Mbps_80ms_ECNEn_BQLDis/"** where "1" indicates the number of flows, "80Mbps" indicates the bottleneck bandwidth, "80ms" indicates the RTT between the n0-server and n0-client, EcnEn indicates that ECN is enabled and BQLDis indicates that BQL is disabled.

Note: The folder names are distinctively identified by the Queue discipline, Bottleneck bandwidth, number of flows, RTT, ECN, and BQL. These parameters have been identified as the important ones under the scope of the experiment. Varying any other parameter and running the simulation would overwrite the folder consisting of dat files.

## Generating a plot:
How we can generate a plot with plotter-gnu can be demonstrated as follows.
Assume we want to compare the following 3 dat files in a single graph: 
1. fqCodel_1_80Mbps_80ms_ECNEn_BQLDis/mark.dat
2. fqPie_1_80Mbps_80ms_ECNEn_BQLDis/mark.dat
3. fqCobalt_1_80Mbps_80ms_ECNEn_BQLDis/mark.dat

In such a case we would have to run the following. Notice that we are running this command in a folder that contains all the three folders (fqCodel_1_80Mbps_80ms_EcnEn_BQLDis/, fqPie_1_80Mbps_80ms_ECNEn_BQLDis/, fqCobalt_1_80Mbps_80ms_ECNEn_BQLDis/). If you run from elsewhere, please adjust the paths accordingly when you run.

```bash
gnuplot -e "files = 'fqCodel_1_80Mbps_80ms_ECNEn_BQLDis/mark.dat fqPie_1_80Mbps_80ms_ECNEn_BQLDis/mark.dat fqCobalt_1_80Mbps_80ms_ECNEn_BQLDis/mark.dat' ; outputfile='MyPlot.png'; titles = 'FQ-CoDel FQ-Pie FQ-Cobalt'; X_axis_label='Time (Seconds)' ; Y_axis_label='ECN Marks'" plotter-gnu
```






